package main

import "fmt"
import "os/exec"
import "runtime"

func Pyrogenesis(arguments ...string) {
	var command = exec.Command(Settings.Pyrogenesis, arguments...)
	if runtime.GOOS == "windows" {
		ClosePublic()
		data, err := command.CombinedOutput()
		fmt.Println(err)
		fmt.Println(string(data))
		ReloadPublic()
	}
	if runtime.GOOS == "linux" {
		fmt.Println(command.Start())
	}
	
}

package main

import "github.com/Splizard/gowut/gwu"
import "fmt"
import "os"
import "sort"

var EditorRefresh func(e gwu.Event)

func uniquify(list []string) []string {
	sort.Strings(list)
	
	for i := range list {
		if i == len(list) {
			break
		}
		
		if i > 0 {
			if list[i] == list[i-1] {
				list[i-1] = list[len(list)-1]
				list = list[:len(list)-1]
			}
		}
	}
	
	sort.Strings(list)
	
	return list
}

func RefreshComponents(entity *Entity, components gwu.Panel, editor gwu.Panel) {
	components.Clear()
	
	Map := entity.Map()
	Keys := make([]string, 0)
	for k, _ := range Map {
		Keys = append(Keys, k)
	}
	sort.Strings(Keys)
	
	for _, key := range Keys {
		component := key
		
		button := gwu.NewButton(component)
		
		button.Style().Set("float", "left")
		components.Add(button)
		
		if !entity.State(component) {
			button.SetText(component+" ❌")
		}
		
		var refresh = func(e gwu.Event) {
			//if e.MouseBtn() == gwu.MouseBtnLeft {
				editor.Clear()
				editor.Style().SetPadding("50px")
				label := gwu.NewLabel(component)
				label.Style().SetFontSize("32px")
				label.Style().SetPaddingBottom("30px")
				
				row := gwu.NewHorizontalPanel()
				
				row.Add(label)
				
				
				DisableButton := gwu.NewButton("✓")
				DisableButton.Style().SetMarginLeft("25px")
				
				if entity.State(component) {
					DisableButton.SetText("✓")
				} else {
					DisableButton.SetText("❌")
				}
				
				DisableButton.AddEHandlerFunc(func(e gwu.Event) {
					e.MarkDirty(DisableButton)
					e.MarkDirty(button)
					
					if DisableButton.Text() == "✓" {
						DisableButton.SetText("❌")
						button.SetText(component+" ❌")
						entity.Disable(component)
					} else {
						DisableButton.SetText("✓")
						button.SetText(component)
						entity.Enable(component)
					}
				}, gwu.ETypeClick)
				
				row.Add(DisableButton)
				
				editor.Add(row)
				editor.SetHAlign(gwu.HALeft)
				
				
				
				//Generate the component editor for this component.
				entity.ComponentEditor(component, editor)
				
				e.MarkDirty(editor)
			//}
		}
			
		button.AddEHandlerFunc(func(e gwu.Event) {
			EditorRefresh = refresh
			refresh(e)
		}, gwu.ETypeClick)
	}
}

func CreateEditorWindow(template string) (Window gwu.Window) {
	
	println(template)
	
	//MainWindow
	Window = gwu.NewWindow("editor", "Entity Editor")
	MoreWidgets(Window).DisableBackButton()
	Window.SetLayout(gwu.LayoutNatural)
	
	Window.AddHeadHTML(`
	
	 <style>
    @font-face {
      font-family: 'ubuntu'; /*Name of Font*/
      src: url('ttf/Ubuntu-R.ttf'); /*URL to Font*/
    }

    body {
      overflow:hidden;
		font-family: "ubuntu", Times, serif;
	}

    /*BACKGROUND*/
    .background{
      position:absolute;
      width:100vw;height:100vh;
      background-size:cover;
      border-radius: 0px;
      top:0px;left:0px;
      background-image:url("png/entityBackground.png");
    }

    /*ENTITY DISPLAY/SELECT*/
    .entitySelection{
      position:absolute;
      top:20px;left:20px;
      width:280px;height:700px;
      background-size:contain;
      background-repeat:no-repeat;
      border-radius: 0px;
      background-image:url("png/entitySelection.png");
    }

    /*ENTITY DISPLAY/3D MODEL*/
    .entityDisplay{
      position:relative;
      background-size:contain;
      top:11%;left:14%;
      height:200px; width:200px;
    }

    /*ENTITY LIST*/
    .entityList {
      position:absolute;
      background-size: contain;
      top:48%;
      height:47%;
	  width: 240px;
		overflow-y: hidden;
		padding-left: 20px;
    }
    /*ENTITY LIST TEXT*/
    .entityList center{
      padding-top: 1%;
      margin-right:10%;
      margin-left:10%;
      padding-bottom:1%;
      font-size: 1.4vmin;
      font-family: "knewave", Times, serif;
    }


    /*ENTITY COMPONENT AREA*/
    .entityComponentArea{
      position:absolute;
      background-size: contain;
      top:12vh; left:320px;
      width: calc(98vw - 320px);
      background-image:url("png/paper.png");
    }

    /*Button for adding components*/
    .addComponent{
      width:4vw; height:4vw;
      background-size:contain;
      background-image:url("png/addComponent.png");
    }

    /*Area that component buttons take up*/
    .buttonArea{
      background-size: contain;
      margin-left:4vw;
      margin-top: -4vw;
      height:12vh;
      overflow-y: scroll;
    }

    /*In this area you can edit component settings*/
    .componentEditor {
      background-size: contain;
      height:72vh;
      overflow-y: scroll;
    }

    /*PAPER BUTTON*/
    button{
      border-radius:5px;
      float:left;
      margin-right:10px;
      margin-bottom:10px;
      background-size:cover;
      width:auto;height:25%;
      background-image:url("png/paperButton.png");
    }
    /*PAPER BUTTON TEXT*/
    button center{
      padding-top: 1%;
      margin-right:4px;
      margin-left:4px;
      padding-bottom:1%;
      font-size: 1.4vmin;
      font-family: "knewave", Times, serif;
    }

    /*ENTITY COMPONENT*/
    .entityComponentsImage{
      position:absolute;
      background-size: contain;
      height:50px;width:400;
      border-radius: 0px;
      top:2vh; left:calc(300px + 20vw);
      background-image:url("png/entityComponents.png");
    }

    /*RIBBONS*/
    .ribbon{
      position:absolute;
      width:5vw;
      border-radius: 0px;
      transition: .5s ease;
      top:calc(-15vh);
    }
    .ribbon:hover{
      transition: .5s ease;
      top:calc(0vh);
    }
    /*Used for positioning home ribbon*/
    .home{
      left:calc(25vw);
    }
    /*Used for positioning create ribbon*/
    .create{
      left:calc(31vw);
    }
    /*Used for positioning save ribbon*/
    .save{
      left:calc(80vw);
    }
    /*Used for positioning run ribbon*/
    .run{
      left:calc(86vw);
    }
	
	
	h2 {
        margin: 0;
        display: inline-block;
        margin-left:40px;
        margin-bottom:10px;
      }

      .nameEntity{
        margin-top:35vh;
        margin-left:40vw;
        width: 350px;
        height: 120px;
        border-style:outset;
        border-width:10px;
        border-radius:10px;
        background: white;
        font-family:'Knewave', cursive;
		position: fixed;
		top: 0;
		left: 0;
        z-index:99999;
      }

      .close{
        margin-left:30px;
        font-family:'Knewave', cursive;
      }

      .text{
        margin-left:30px;
      }

      .createPopup {
        margin-top:10px;
        margin-left:40%;
        font-family:'Knewave', cursive;
      }

    </style>
	
	`)
	
	var ComponentButtons gwu.Panel
	var Editor gwu.Panel
	var ModelViewer = gwu.NewNaturalPanel()
	var entity *Entity
	
	if template != "" {
		var err error
		
		entity, err = LoadEntity(template)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		
		actor, err := LoadActor(entity.Actor())
		if err != nil {
			fmt.Println(err.Error())
		}
		
		//Main Editor, content will change when a compnent is clicked.
		Editor = gwu.NewPanel()
		Editor.SetHAlign(gwu.HALeft)
		
		//Components.
		ComponentButtons = gwu.NewNaturalPanel()
		RefreshComponents(entity, ComponentButtons, Editor)
		
		ModelViewer.Add(NewModelViewer(actor))
		//ComponentButtons = BuildComponentButtons()
	}
	
	var templates = uniquify(append(Public.Templates(), Settings.Mod.Templates()...))
	
	
	var TemplatesHTML = "<option></option>"
	for _, template := range templates {
		TemplatesHTML += "<option>"+template+"</option>"
	}
	
	ListBox := gwu.NewListBox(templates)
	ListBox.AddEHandlerFunc(func(e gwu.Event) {		
		if ListBox.SelectedIdx() > -1 {
			Server.RemoveWin(Window)
			Server.AddWin(CreateEditorWindow(templates[ListBox.SelectedIdx()]))
			e.ReloadWin("editor")
		}
		
	}, gwu.ETypeChange)
	ListBox.SetMulti(true)
	ListBox.Style().AddClass("selectEntityList")
	ListBox.Style().Set("height", "200px")
	
	var elements = RawElements()
	elements["ModelViewer"] = ModelViewer
	elements["ListBox"] = ListBox
	
	Raw(Window).Render(elements, `
	
	 <!--BACKGROUND IMAGE-->
    <div class="background"></div>

    <!--ENTITY SELECTION/DISPLAY-->
    <div class="entitySelection">
      <!--ENTITY LIST-->
      <div class="entityList">
		`+template+`
		<input type="text" id="searchEntityList" onkeyup="searchEntities()" placeholder="Search templates..">
		
		{{ListBox}}
      </div>
      <!--ENTITY DISPLAY-->
      <div class="entityDisplay">
		{{ModelViewer}}
      </div>
    </div>

    <!--THE AREA THE ENTITY COMPONENT FITS INTO-->
    <div class="entityComponentArea">
    
    <div class="addComponent"></div>

      <!--AREA THAT THE BUTTONS TAKE UP-->
      <div class="buttonArea">
          <!--COMPONENT BUTTONS-->
	
	`)
	
	if ComponentButtons != nil {
		Window.Add(ComponentButtons)
	}
	
	Window.Add(gwu.NewHTML(`
      </div>

      <!--COMPONENT EDITOR-->
      <div class="componentEditor">
      
      `))
	
	if Editor != nil {
		Window.Add(Editor)
	}
      
     Window.Add(gwu.NewHTML(`
      
      </div>

    </div>

    <!--COMPONENTS IMAGE-->
    <div class="entityComponentsImage"></div>


	`))
	 
	 Popup := gwu.NewNaturalPanel()
	 Popup.Style().AddClass("nameEntity")
	 
	 Popup.Add(gwu.NewHTML(` <div>
      <h2>Entity From Template</h2>
      <button class="close" onclick="document.getElementById(`+Popup.ID().String()+`).style.display = 'none'">X</button>
    </div>`))
	 
	 if template != "" {
	 Popup.Add(gwu.NewLabel("Name the entity"))
	 {
		TextBox := gwu.NewTextBox(template+"_child")
		Popup.Add(TextBox)
		Button := gwu.NewButton("Create")
		
		Button.AddEHandlerFunc(func(e gwu.Event) {
			
			println("Creating new entity!")
			println(TextBox.Text())
			NewEntity(template).WriteToFile(Mods+Templates+TextBox.Text())
			
			Server.RemoveWin(Window)
			Server.AddWin(CreateEditorWindow(TextBox.Text()))
			e.ReloadWin("editor")
			
			
		}, gwu.ETypeClick)
		
		Popup.Add(Button)
	 
	 }
	 }

	Popup.Style().Set("display", "none")
	 
	 {
		Image := NewImageButton("Entity Editor", "png/entityCard.png", func(e gwu.Event) {		
			Server.RemoveWin(Window)
			Server.AddWin(CreateEditorWindow(""))
			e.ReloadWin("editor")
		})
		Image.Style().AddClass("entity").AddClass("card")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Save", "png/save.png", func(e gwu.Event) {
			if entity != nil {
				entity.WriteToFile(Mods+Templates+template) 
			}
		})
		Image.Style().AddClass("ribbon").AddClass("save")
		Window.Add(Image)
		
		Window.Add(gwu.NewHTML(`<script>window.addEventListener("keypress", function(event) {
				if (!(event.which == 115 && event.ctrlKey) && !(event.which == 19)) return true;
				se(new MouseEvent('click', {}), 0, `+Image.ID().String()+`);
				event.preventDefault();
				return false;
		})</script>`))
	}
	
	{
		Image := NewImageButton("Home", "png/home.png", func(e gwu.Event) {		
			Server.RemoveWin(Window)
			Server.AddWin(CreateStartupWindow())
			e.ReloadWin("startup")
		})
		Image.Style().AddClass("ribbon").AddClass("home")
		Window.Add(Image)
	}
	
	{
		
		Image := NewImageButton("Create", "png/create.png", func(e gwu.Event) {		
			Popup.Style().Set("display", "initial")
			e.MarkDirty(Popup)
		})
		Image.Style().AddClass("ribbon").AddClass("create")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Run", "png/run.png", func(e gwu.Event) {		
			Pyrogenesis("-mod=public",  "-mod="+Settings.Mod.Name, "-autostart=skirmishes/Acropolis Bay (2)", "-autostart-civ=1:athen")
		})
		Image.Style().AddClass("ribbon").AddClass("run")
		Window.Add(Image)
	}
	
	Select := gwu.NewHTML(`
		<script>
			function searchEntities() {
				console.log("searching");
				// Declare variables
				var input, filter, ul, li, a, i;
				input = document.getElementById('searchEntityList');
				filter = input.value.toUpperCase();
				ul = document.getElementById("`+ListBox.ID().String()+`");
				li = ul.getElementsByTagName('option');

				// Loop through all list items, and hide those who don't match the search query
				for (i = 0; i < li.length; i++) {
					if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
						li[i].style.display = "";
					} else {
						li[i].style.display = "none";
					}
				}
			}
	</script>`)
		Window.Add(Select)
		
		Window.Add(Popup)
	
	
	/*if template != "" {
		entity, err := LoadEntity(template)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		
		actor, err := LoadActor(entity.Actor())
		if err != nil {
			fmt.Println(err.Error())
		}
		
		//Main Editor, content will change when a compnent is clicked.
		editor := gwu.NewPanel()
		editor.SetHAlign(gwu.HALeft)
		
		//Components.
		components := gwu.NewNaturalPanel()
		RefreshComponents(entity, components, editor)
		
		Window.Add(components)
		Window.Add(editor)
		
		
		
		ModelViewer := NewModelViewer(actor)
		Window.Add(ModelViewer)
		
		Window.Add(gwu.NewHTML(`

		<div class="modelEditor">
			<p> </p>
			<center>`+template+`</center>
		</div>
		
		<button type="button" class="back">←</button>
		<button type="button" class="add">+</button>
		<form method="post" action="startup">
		<button type="submit" class="home">HOME</button>
		</form>
		
		<div class="edit">
			<center>Component Editor</center>
		</div>
		`))
		
		////
		
			//Hidden ComponentPalette
			Palette := gwu.NewNaturalPanel()
			Palette.Style().Set("display", "none")
			Palette.Style().Set("position", "fixed")
			Palette.Style().Set("top", "0")
			Palette.Style().Set("left", "0")
			Palette.Style().Set("width", "100vw")
			Palette.Style().Set("height", "100vh")
			Palette.Style().Set("background-color", "white")
			Palette.Style().Set("z-index", "9999")
			
			Button := gwu.NewButton("+")
			Button.Style().SetClass("add")
					
			Button.AddEHandlerFunc(func(e gwu.Event) {
				if e.MouseBtn() == gwu.MouseBtnLeft {
					
					Palette.Style().Set("display", "none")
					e.MarkDirty(Palette)
					
				}
			}, gwu.ETypeClick)

				
			for i  := range ComponentPalette.Root().ChildElements() {
				child := ComponentPalette.Root().ChildElements()[i]
				
				//In order to support options.
				button := gwu.NewButton(child.Tag)
				if i > 0 {
					last := ComponentPalette.Root().ChildElements()[i-1]
					if last.Tag == child.Tag {
						button.SetText(child.Tag+child.ChildElements()[0].Tag)
					}
				}
				if i < len(ComponentPalette.Root().ChildElements())-2 {
					next := ComponentPalette.Root().ChildElements()[i+1]
					if next.Tag == child.Tag {
						button.SetText(child.Tag+child.ChildElements()[0].Tag)
					}
				}
				
				
				button.AddEHandlerFunc(func(e gwu.Event) {
					if e.MouseBtn() == gwu.MouseBtnLeft {
						
						entity.Components.AddChild(child)
						RefreshComponents(entity, components, editor)
						
						Palette.Style().Set("display", "none")
						e.MarkDirty(Palette)
						
						e.MarkDirty(components)
					}
				}, gwu.ETypeClick)
				
				Palette.Add(button)
			}
			Palette.Add(Button)
			
			Window.Add(Palette)
			
		////
			
			
		//Add Button!
		Button = gwu.NewButton("+")
		Button.Style().SetClass("add")
				
		Button.AddEHandlerFunc(func(e gwu.Event) {
			if e.MouseBtn() == gwu.MouseBtnLeft {
				
				Palette.Style().Set("display", "initial")
				e.MarkDirty(Palette)
				
			}
		}, gwu.ETypeClick)
		Window.Add(Button)
		
		SaveButton = gwu.NewButton("Save")
		SaveButton.Style().SetClass("save")
				
		SaveButton.AddEHandlerFunc(func(e gwu.Event) {
			SaveButton.SetText("Saved ✓")
			e.MarkDirty(SaveButton)
			
			entity.WriteToFile(Mods+Templates+template)
		}, gwu.ETypeClick)
		Window.Add(SaveButton)
		
		Window.Add(gwu.NewHTML(`<script>window.addEventListener("keypress", function(event) {
			if (!(event.which == 115 && event.ctrlKey) && !(event.which == 19)) return true;
			se(new MouseEvent('click', {}), 0, `+SaveButton.ID().String()+`);
			event.preventDefault();
			return false;
	})</script>`))
	}
	
	//Home button.
	Button := gwu.NewButton("Home")
	Button.Style().SetClass("home")
			
	Button.AddEHandlerFunc(func(e gwu.Event) {
		if e.MouseBtn() == gwu.MouseBtnLeft {
			Server.RemoveWin(Window)
			Server.AddWin(CreateStartupWindow())
			e.ReloadWin("startup")
		}
	}, gwu.ETypeClick)
	Window.Add(Button)
	
	Button = gwu.NewButton("Run")
	Button.Style().SetClass("run")
			
	Button.AddEHandlerFunc(func(e gwu.Event) {
		if e.MouseBtn() == gwu.MouseBtnLeft {
			Pyrogenesis("-mod=public",  "-mod="+ActiveMod.Name, "-autostart=skirmishes/Acropolis Bay (2)", "-autostart-civ=1:athen")
		}
	}, gwu.ETypeClick)
	Window.Add(Button)*/
	
	return
}

package main

import "github.com/Splizard/gowut/gwu"

var HidePublic bool

func CreateStartupWindow() (Window gwu.Window) {
	Window = gwu.NewWindow("startup", "0ed")
	
	
	Window.AddHeadHTML(`
		<script src="js/jquery.min.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		
		<style>
			body {
				background-image: url("png/background.png");
				background-size: cover;
				background-repeat: no-repeat;
				overflow-y: hidden;
			}
			
			/*CARD CLASS*/
			.card{
				position:absolute;
				width:15vw;
				border-radius: 0px;
				transition: .5s ease;
				top:calc(64vh);
			}
			.card:hover{
				transition: .5s ease;
				top:calc(50vh);
			}
			/*Used for positioning*/
			.entity{
				left:calc(10vw);
			}
			.visual{
				left:calc(26vw);
			}
			.audio{
				left:calc(42vw);
			}
			.map{
				left:calc(58vw);
			}
			.code{
				left:calc(74vw);
			}
		
			/*RIBBONS*/
    .ribbon{
      position:absolute;
      width:10vw;
      border-radius: 0px;
      transition: .5s ease;
      top:calc(-30vh);
      left:calc(86vw);
    }
    .ribbon:hover{
      /* Start the shake animation and make the animation last for 0.5 seconds */
    animation: shake 0.5s;
    /* When the animation is finished, start again */
    animation-iteration-count: infinite;
    }
	
	@keyframes shake {
    0% { transform: translate(1px, 1px) rotate(0deg); }
    10% { transform: translate(-1px, -2px) rotate(-1deg); }
    20% { transform: translate(-3px, 0px) rotate(1deg); }
    30% { transform: translate(3px, 2px) rotate(0deg); }
    40% { transform: translate(1px, -1px) rotate(1deg); }
    50% { transform: translate(-1px, 2px) rotate(-1deg); }
    60% { transform: translate(-3px, 1px) rotate(0deg); }
    70% { transform: translate(3px, 1px) rotate(-1deg); }
    80% { transform: translate(-1px, -1px) rotate(1deg); }
    90% { transform: translate(1px, 2px) rotate(0deg); }
    100% { transform: translate(1px, -2px) rotate(-1deg); }
}

		</style>
    
	`)
	
	/*Select := gwu.NewHTML(`
	<script>
	
	
		$(document).ready(function() {
		
			$.fn.select2.defaults.set('matcher', function(params, data) {
				// If there are no search terms, return all of the data
				if ($.trim(params.term) === '') {
					return data;
				}

				// Do not display the item if there is no 'text' property
				if (typeof data.text === 'undefined') {
					return null;
				}

				var words = params.term.toUpperCase().split(" ");

				for (var i = 0; i < words.length; i++) {
					if (data.text.toUpperCase().indexOf(words[i]) < 0) {
					return null;
					}
				}

				return data;
			});
		
			$('.selectpicker').select2();
		});
	
	
</script>`)*/
	
	MoreWidgets(Window).DisableBackButton()
	
	/*var templates = Public.Templates()
	
	ListBox := gwu.NewListBox(append([]string{""}, templates...))
	ListBox.SetMulti(false)
		ListBox.Style().Set("position", "absolute").
		Set("top", "calc(100vh / 2 - 180px)").
		Set("left", "calc(100vw / 2 - 400px)").
		Set("z-index", "99").
		SetWidthPx(300).
		SetHeightPx(380)
	Window.Add(ListBox)
	ListBox.Style().AddClass("selectpicker")
	
	
	Window.Add(gwu.NewHTML(`
    <div class="actor">
         <center>Select an actor</center>
    </div>
    <div class="model">
         <center>Model</center>
    </div>
  `))
	
		Button := gwu.NewButton("Accept")
	Button.Style().SetClass("accept")
			
	Button.AddEHandlerFunc(func(e gwu.Event) {
		if e.MouseBtn() == gwu.MouseBtnLeft {
			Server.RemoveWin(Window)
			Server.AddWin(CreateEditorWindow(ListBox.SelectedValue()))
			e.ReloadWin("editor")
		}
	}, gwu.ETypeClick)
	Window.Add(Button)
	
	var components = Public.Components()
	
	ComponentsListBox := gwu.NewListBox(components)

	//New ModManager() {
		
	
		var mod_manager = gwu.NewHorizontalPanel()
		listbox := gwu.NewListBox(append([]string{ActiveMod.Name}, OtherMods()...))
		Window.Add(gwu.NewLabel("Active Mod:"))
		mod_manager.Add(listbox)
		
		listbox.AddEHandlerFunc(func(e gwu.Event) {
			NewMod(listbox.SelectedValue())
		}, gwu.ETypeChange)
		
		var new_mod_name = gwu.NewTextBox("")
		
		mod_manager.Add(new_mod_name)
		
		var new_mod = gwu.NewButton("New")
		
		new_mod.AddEHandlerFunc(func(e gwu.Event) {
			if e.MouseBtn() == gwu.MouseBtnLeft {
				
				NewMod(new_mod_name.Text())
				listbox.SetValues(append([]string{ActiveMod.Name}, OtherMods()...))
				
				e.MarkDirty(listbox)
			}
		}, gwu.ETypeClick)
		
		mod_manager.Add(new_mod)

		CheckBox := gwu.NewCheckBox("Hide Public")
		CheckBox.SetState(HidePublic)
		mod_manager.Add(CheckBox)
		
		if HidePublic {
			ListBox.SetValues(ActiveMod.Templates())
			ComponentsListBox.SetValues(ActiveMod.Components())	
		}
		
		CheckBox.AddEHandlerFunc(func(e gwu.Event) {
			if e.MouseBtn() == gwu.MouseBtnLeft {
				
				HidePublic = CheckBox.State()
				
				if CheckBox.State() {
					e.MarkDirty(ListBox)
					e.MarkDirty(Select)
					ListBox.SetValues(ActiveMod.Templates())
					
					e.MarkDirty(ComponentsListBox)
					ComponentsListBox.SetValues(ActiveMod.Components())
				} else {
					e.MarkDirty(ListBox)
					e.MarkDirty(Select)
					ListBox.SetValues(Public.Templates())
					
					e.MarkDirty(ComponentsListBox)
					ComponentsListBox.SetValues(Public.Components())
				}
			}
		}, gwu.ETypeClick)
		
		Window.Add(mod_manager)
	// }
	
	
	Window.Add(gwu.NewLabel("Component Editor"))
	
	
	Button = gwu.NewButton("Edit Component")
			
	Button.AddEHandlerFunc(func(e gwu.Event) {
		if ComponentsListBox.SelectedValue() == "" {
			return
		}
		
		if e.MouseBtn() == gwu.MouseBtnLeft {
			Server.RemoveWin(Window)
			Server.AddWin(CreateComponentEditorWindow(ComponentsListBox.SelectedValue()))
			e.ReloadWin("ceditor")
		}
	}, gwu.ETypeClick)
	Window.Add(Button)
	
	
	ComponentsListBox.SetMulti(true)
	ComponentsListBox.Style().SetWidthPx(200)
	ComponentsListBox.Style().SetHeightPx(800)
	Window.Add(ComponentsListBox)
	
	Window.Add(Select)*/
	
	{
		Image := NewImageButton("Entity Editor", "png/entityCard.png", func(e gwu.Event) {		
			Server.RemoveWin(Window)
			Server.AddWin(CreateEditorWindow(""))
			e.ReloadWin("editor")
		})
		Image.Style().AddClass("entity").AddClass("card")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Visual Editor", "png/visualCard.png", func(e gwu.Event) {		
			println("Unimplemented!")
		})
		Image.Style().AddClass("visual").AddClass("card")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Audio Editor", "png/audioCard.png", func(e gwu.Event) {		
			println("Unimplemented!")
		})
		Image.Style().AddClass("audio").AddClass("card")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Map Editor", "png/mapCard.png", func(e gwu.Event) {		
			Pyrogenesis("--editor")
		})
		Image.Style().AddClass("map").AddClass("card")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Code Editor", "png/codeCard.png", func(e gwu.Event) {		
			Server.RemoveWin(Window)
			Server.AddWin(CreateComponentEditorWindow(""))
			e.ReloadWin("ceditor")
		})
		Image.Style().AddClass("code").AddClass("card")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Settings Editor", "png/settings.png", func(e gwu.Event) {		
			Server.RemoveWin(Window)
			Server.AddWin(CreateSettingsWindow())
			e.ReloadWin("seditor")
		})
		Image.Style().AddClass("ribbon")
		Window.Add(Image)
	}	
	
	return
}

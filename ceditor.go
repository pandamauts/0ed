package main

import "github.com/Splizard/gowut/gwu"
import "fmt"
import "os"
import "path/filepath"

import "io/ioutil"


func CreateComponentEditorWindow(path string) (Window gwu.Window) {
	//MainWindow
	Window = gwu.NewWindow("ceditor", "Component Editor")
	MoreWidgets(Window).DisableBackButton()
	Window.SetLayout(gwu.LayoutNatural)
	
	Window.AddHeadHTML(`
		
		<style>
			@font-face {
			font-family: 'knewave'; /*Name of Font*/
			src: url('ttf/Knewave-Regular.ttf'); /*URL to Font*/
			}

			body {
			overflow:hidden;
			}

			/*BACKGROUND*/
			.background{
			position:absolute;
			width:100vw;height:100vh;
			background-size:cover;
			border-radius: 0px;
			top:0px;left:0px;
			background-image:url("png/codeBackground.png");
			}

			/*CODE SELECTION AREA*/
			.codeSelection{
				position:absolute;
				top:20px;left:20px;
				width:280px;height:700px;
				background-size:contain;
				background-repeat:no-repeat;
				border-radius: 0px;
				background-image:url("png/selection.png");
			}

			/*CODE LIST*/
			.codeList {
				position:absolute;
				background-size: contain;
				top:10%;
				height:85%;
				padding: 22px;
			}
			/*CODE LIST TEXT*/
			.codeList center{
			padding-top: 1%;
			margin-right:10%;
			margin-left:10%;
			padding-bottom:1%;
			font-size: 2vmin;
			font-family: "knewave", Times, serif;
			}

			/*CODE EDITING AREA*/
			.codeEditingArea{
			position:absolute;
			background-size: contain;
			top:12vh; left:320px;
			width: calc(98vw - 320px);
			background-image:url("png/stone.png");
			}

			/*CODE EDITOR*/
			.codeEditor {
			background-size: contain;
			background-color: white;
			height:84vh;
			overflow-y: scroll;
			}
			/*CODE LIST TEXT*/
			.codeEditor center{
			font-size: 2vmin;
			font-family: "knewave", Times, serif;
			}

			/*CODE EDITING IMAGE*/
			.codeEditorImage{
				position:absolute;
				background-size: contain;
				height:50px;width:400;
				border-radius: 0px;
				top:-10%; left:20%;
				background-image:url("png/codeEditor.png");
			}

			/*RIBBONS*/
			.ribbon{
				position:absolute;
				width:5vw;
				border-radius: 0px;
				transition: .5s ease;
				top:calc(-15vh);
				z-index: 999;
			}
			.ribbon:hover{
				transition: .5s ease;
				top:calc(0vh);
			}
			/*Used for positioning home ribbon*/
				.home{
				left:calc(25vw);
			}
			/*Used for positioning save ribbon*/
			.save{
				left:calc(80vw);
			}
			/*Used for positioning run ribbon*/
			.run{
				left:calc(86vw);
			}


			</style>
		
		<script src="js/codemirror.js"></script>
		<link rel="stylesheet" href="js/codemirror.css">
		<script src="js/javascript.js"></script>
		
		`)
	
	var data []byte
	
	if path != "" {
		
		file, err := Settings.Mod.Open(Components+path+".js")
		if err != nil {
			Window.Add(gwu.NewLabel(fmt.Sprint(err)))
			return
		}
		defer file.Close()
		
		data, err = ioutil.ReadAll(file)
		if err != nil {
			Window.Add(gwu.NewLabel(fmt.Sprint(err)))
			return
		}
		
	}
	
	Code := gwu.NewTextBox(string(data))
	Code.SetRows(2)
	CodeEditor := gwu.NewPanel()
	CodeEditor.Add(Code)
	CodeEditor.Add(gwu.NewHTML(`
		<script>
		var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("`+Code.ID().String()+`"), {
			lineNumbers: true,
		});
		myCodeMirror.setSize("100%", "100%");
		myCodeMirror.on("change", function() {
			myCodeMirror.save();
			document.getElementById("`+Code.ID().String()+`").onchange();
		}
		)
		</script>
		`))
	Code.AddEHandlerFunc(func(e gwu.Event) {
		if path != "" {
			os.MkdirAll(filepath.Dir(Mods+Components+path+".js"), 0755)
			ioutil.WriteFile(Mods+Components+path+".js", []byte(Code.Text()), 0755)
		}
	}, gwu.ETypeChange)
	
	var components = uniquify(append(Public.Components(), Settings.Mod.Components()...))
	ComponentsListBox := gwu.NewListBox(components)
	ComponentsListBox.SetMulti(true)
	ComponentsListBox.Style().Set("height", "100%").Set("max-height", "calc(100vh - 20%)")
	ComponentsListBox.AddEHandlerFunc(func(e gwu.Event) {
		Server.RemoveWin(Window)
		Server.AddWin(CreateComponentEditorWindow(ComponentsListBox.SelectedValue()))
		e.ReloadWin("ceditor")
	}, gwu.ETypeChange)
	
	var elements = RawElements()
	elements["CodeEditor"] = CodeEditor
	elements["SelectCode"] = ComponentsListBox
	
	Raw(Window).Render(elements, `<!--BACKGROUND IMAGE-->
    <div class="background"></div>

    <!--CODE SELECTION AREA-->
    <div class="codeSelection">
      <!--SELECT CODE-->
      <div class="codeList">
        {{SelectCode}}
      </div>
    </div>

    <!--CODE EDITING AREA-->
    <div class="codeEditingArea">
      <!--CODE EDITOR WORDS-->
      <div class="codeEditorImage"></div>
      <!--CODE EDITOR-->
      <div class="codeEditor">
		{{CodeEditor}}
      </div>
    </div>
    
    <img id="saveButton" src="png/save.png" alt="ENTITY" class="ribbon save">
    
    <script type="text/javascript">

		window.onload = function() {

			document.getElementById('saveButton').addEventListener('click', function (e) {
				myCodeMirror.save();
				document.getElementById("`+Code.ID().String()+`").onchange();
			});

		};
		
		window.addEventListener("keypress", function(event) {
				if (!(event.which == 115 && event.ctrlKey) && !(event.which == 19)) return true;
					   
				myCodeMirror.save();
				document.getElementById("`+Code.ID().String()+`").onchange();
	
				event.preventDefault();
				return false;
		})

	</script>
    `)
	
	{
		Image := NewImageButton("Home", "png/home.png", func(e gwu.Event) {		
			Server.RemoveWin(Window)
			Server.AddWin(CreateStartupWindow())
			e.ReloadWin("startup")
		})
		Image.Style().AddClass("ribbon").AddClass("home")
		Window.Add(Image)
	}
	
	{
		Image := NewImageButton("Run", "png/run.png", func(e gwu.Event) {		
			Pyrogenesis("-mod=public",  "-mod="+Settings.Mod.Name, "-autostart=skirmishes/Acropolis Bay (2)", "-autostart-civ=1:athen")
		})
		Image.Style().AddClass("ribbon").AddClass("run")
		Window.Add(Image)
	}
	
	/*Buttons := gwu.NewHorizontalPanel()
	
	Button := gwu.NewButton("Home")
	//Button.Style().SetClass("home")
			
	Button.AddEHandlerFunc(func(e gwu.Event) {
		if e.MouseBtn() == gwu.MouseBtnLeft {
			Server.RemoveWin(Window)
			Server.AddWin(CreateStartupWindow())
			e.ReloadWin("startup")
		}
	}, gwu.ETypeClick)
	Buttons.Add(Button)
	
	var components = Public.Components()
	ComponentsListBox := gwu.NewListBox(components)
	
	ComponentsListBox.AddEHandlerFunc(func(e gwu.Event) {
		Server.RemoveWin(Window)
		Server.AddWin(CreateComponentEditorWindow(ComponentsListBox.SelectedValue()))
		e.ReloadWin("ceditor")
	}, gwu.ETypeChange)
	
	ComponentsListBox.SetMulti(true)
	ComponentsListBox.Style().SetWidthPx(200)
	ComponentsListBox.Style().SetHeightPx(800)
	
	SideBar := gwu.NewPanel()
	
	if path != "" {
		
		file, err := ActiveMod.Open(Components+path+".js")
		if err != nil {
			Window.Add(gwu.NewLabel(fmt.Sprint(err)))
			return
		}
		defer file.Close()
		
		data, err := ioutil.ReadAll(file)
		if err != nil {
			Window.Add(gwu.NewLabel(fmt.Sprint(err)))
			return
		}
		
		Code := gwu.NewTextBox(string(data))
		
		Button = gwu.NewButton("Run")
		Button.Style().SetClass("run")
				
		Button.AddEHandlerFunc(func(e gwu.Event) {
			if e.MouseBtn() == gwu.MouseBtnLeft {
				Pyrogenesis("-mod=public",  "-mod="+ActiveMod.Name, "-autostart=skirmishes/Acropolis Bay (2)", "-autostart-civ=1:athen")
			}
		}, gwu.ETypeClick)
		Buttons.Add(Button)
		
		SaveButton := gwu.NewButton("Save")
		SaveButton.Style().SetClass("save")
				
		SaveButton.AddEHandlerFunc(func(e gwu.Event) {
			SaveButton.SetText("Saved ✓")
			e.MarkDirty(SaveButton)
			
			os.MkdirAll(filepath.Dir(Mods+Components+path+".js"), 0755)
			ioutil.WriteFile(Mods+Components+path+".js", []byte(Code.Text()), 0755)
		}, gwu.ETypeClick)
		Buttons.Add(SaveButton)
		
		Window.Add(gwu.NewHTML(`<script>window.addEventListener("keypress", function(event) {
			if (!(event.which == 115 && event.ctrlKey) && !(event.which == 19)) return true;
			se(new MouseEvent('click', {}), 0, `+SaveButton.ID().String()+`);
			event.preventDefault();
			return false;
	})</script>`))
		
		
		Code.Style().SetWidth("99vw")
		Code.Style().SetHeight("90vh")
		Code.SetRows(2)
		
		Code.AddEHandlerFunc(func(event gwu.Event) {
			SaveButton.SetText("Save")
			event.MarkDirty(SaveButton)
		}, gwu.ETypeChange, gwu.ETypeKeyUp)

		CodeEditor.Add(Code)
		

		CodeEditor.Add(gwu.NewHTML(`
		<script>
		var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("`+Code.ID().String()+`"), {
			lineNumbers: true,
		});
		myCodeMirror.setSize("100%", "100%");
		myCodeMirror.on("change", function() {
			myCodeMirror.save();
			document.getElementById("`+Code.ID().String()+`").onchange();
		}
		)
		</script>
		`))
	}
	
	SideBar.Add(Buttons)
	SideBar.Add(ComponentsListBox)
	
	Window.SetLayout(gwu.LayoutHorizontal)
	Window.Add(SideBar)
	Window.Add(CodeEditor)*/
	
	return
}

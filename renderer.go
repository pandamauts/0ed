package main

import "github.com/Splizard/gowut/gwu"
import "strings"

type RawPanel struct {
	panel gwu.Panel
}

func Raw(p gwu.Panel) RawPanel {
	return RawPanel{
		panel: p,
	}
}

func RawElements() map[string]gwu.Comp {
	return make(map[string]gwu.Comp)
}

func (r RawPanel) Render(elements map[string]gwu.Comp, html string) {
	
	var splits = strings.Split(html, "{{")
	var panel = r.panel
	
	if len(splits) > 1 {
		
		panel.Add(gwu.NewHTML(splits[0]))
		
		for _, split := range splits[1:] {
			
			var tmp = strings.Split(split, "}}")
			
			if len(tmp) > 1 {
				panel.Add(elements[tmp[0]])
				panel.Add(gwu.NewHTML(tmp[1]))
			} else {
				panel.Add(gwu.NewHTML(tmp[0]))
			}
		}
		
	} else {
		panel.Add(gwu.NewHTML(html))
	}
}

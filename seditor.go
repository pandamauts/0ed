package main

import "github.com/Splizard/gowut/gwu"

func CreateSettingsWindow() (Window gwu.Window) {
	Window = gwu.NewWindow("seditor", "Settings Editor")
	MoreWidgets(Window).DisableBackButton()
	Window.SetLayout(gwu.LayoutNatural)
	
	Window.AddHeadHTML(`
	
	<style>
    @font-face {
      font-family: 'knewave'; /*Name of Font*/
      src: url('./Knewave-Regular.ttf'); /*URL to Font*/
    }

    body {
      overflow:hidden;
    }

    /*BACKGROUND*/
    .background{
      position:absolute;
      width:100vw;height:100vh;
      background-size:cover;
      border-radius: 0px;
      top:0px;left:0px;
      background-image:url("png/settingsBackground.png");
    }

    /*Settings*/
    .settingsEditingArea{
      position:absolute;
      background-size: contain;
      top:12vh; left:2vw;
      width: calc(98vw - 320px);
      background-image:url("png/stone.png");
    }

    /*Settings Container*/
    .settingsEditor {
	  padding: 20px;
      background-size: contain;
      height:84vh;
      overflow-y: scroll;
    }
    /*Settings Font style and size*/
    .settingsEditor center{
      font-size: 2vmin;
      font-family: "knewave", Times, serif;
    }

    /*Settings title image*/
    .settingsEditorImage{
      position:absolute;
      background-size: contain;
      height:50px;width:400;
      border-radius: 0px;
      top:-10%; left:20%;
      background-image:url("png/settingsEditor.png");
    }

    /*RIBBONS*/
    .ribbon{
      position:absolute;
      width:12vw;
      border-radius: 0px;
      transition: .5s ease;
      top:calc(-35vh);
    }
    .ribbon:hover{
      transition: .5s ease;
      top:calc(0vh);
    }
    /*Used for positioning home ribbon*/
    .home{
      left:calc(85vw);
    }


    </style>
	
	`)
	
	
	Panel := gwu.NewPanel()
	Panel.Add(gwu.NewLabel("Pyrogenesis"))
	{
		TextBox := gwu.NewTextBox(Settings.Pyrogenesis)
		TextBox.AddEHandlerFunc(func(e gwu.Event) {
			
			Settings.Pyrogenesis = TextBox.Text()
			
		}, gwu.ETypeChange)
		Panel.Add(TextBox)
	}
	Panel.Add(gwu.NewLabel("Mod"))
	{
		TextBox := gwu.NewTextBox(Settings.Mod.Name)
		TextBox.AddEHandlerFunc(func(e gwu.Event) {
			
			Settings.Mod = &Mod{Name: TextBox.Text()}
			Settings.Mod.Load()
			
		}, gwu.ETypeChange)
		Panel.Add(TextBox)
	}
	
	var elements = RawElements()
	elements["Settings"] = Panel
	
	Raw(Window).Render(elements, `

		 <!--BACKGROUND IMAGE-->
		<div class="background"></div>

		<!--Settings-->
		<div class="settingsEditingArea">
		<!--"Settings" Word Image-->
		<div class="settingsEditorImage"></div>
		<!--The area that all the settings go into-->
		<div class="settingsEditor">
			{{Settings}}
		</div>
		</div>

	`)
	
	{
		Image := NewImageButton("Home", "png/home.png", func(e gwu.Event) {		
			Server.RemoveWin(Window)
			Server.AddWin(CreateStartupWindow())
			e.ReloadWin("startup")
		})
		Image.Style().AddClass("ribbon").AddClass("home")
		Window.Add(Image)
	}	
	
	return
}

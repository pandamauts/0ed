package main

import "github.com/Splizard/gowut/gwu"
import "github.com/Splizard/ctrls"
import "os/user"
import "runtime"

const Templates = "simulation/templates/"
const Portraits = "art/textures/ui/session/portraits/"
const Actors = "art/actors/"
const Meshes = "art/meshes/"
const Sounds = "audio/"
const Textures = "art/textures/skins/"
const Components = "simulation/components/"
const Github = "https://raw.githubusercontent.com/0ad/0ad/master/binaries/data/mods/public/"

var Server gwu.Server

var Settings = struct {
	Pyrogenesis string
	Mod *Mod
}{Pyrogenesis: "pyrogenesis", Mod: &Mod{Name: "0ed"}}

var SettingsPath = "0ed.settings"

func init() {
	uid, _ := user.Current()
	
	if runtime.GOOS == "windows" {
		SettingsPath = uid.HomeDir+"/AppData/Local/0ed/"+SettingsPath
	}
	
	if runtime.GOOS == "linux" {
		SettingsPath = uid.HomeDir+"/.config/0ed/"+SettingsPath
	}
}

func main() {
	ctrls.Load(&Settings, SettingsPath)
	
	Settings.Mod.Load()
	
	//GUI
	Server = gwu.NewServer("0ed", "localhost:8081")
	Server.SetText("0ed")
	
	Server.AddStaticDir("js", "./data/js")
	Server.AddStaticDir("png", "./data/png")
	Server.AddStaticDir("ttf", "./data/ttf")
	
	Server.AddWin(CreateStartupWindow())
	
	Server.Start("startup") // Also opens windows list in browser
}
